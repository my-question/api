# My Question API

## Tecnologias utilizadas
RESTful usando Node.js, Express, Mysql e Sequelize.

## Postman
[**Documentation in Postman**](https://documenter.getpostman.com/view/16658273/2s8YKGjgpH)

**_Coleção disponível na raiz do projeto_**

## Arquitetura
Para a criação do projeto foi utilizado o padrão arquitetural de software MVC.


## Siga os passos abaixo para executar o projeto

1) Crie um banco de dados

2) Renomeie o arquivo da raiz do projeto chamado .env.example para .env e neste defina seus dados de acesso ao banco de dados, a porta para o servidor rodar, a chave para o token JWT em access_secret e a URL em que o web app irá rodar

3) Inicie as dependências do projeto
```
 yarn install
```

4) Execute as migrações para criação das tabelas no banco de dados
```
yarn run migrate
```

5) OPICIONAL) Caso queira você pode popular o banco de dados com as seeds fornecidas
```
yarn run seed
```

6) Inicie o servidor
```
yarn run dev
``` 

**_Vide seção scripts em package.json na raiz do projeto para conhecimento de comandos auxiliares._**

## Documentação
Mais informações do projeto [**aqui**](https://gitlab.com/my-question/doc).

## Estrutura de diretórios

```
├── /src
|   ├── /controllers
|   ├── /database
|   |   ├── /config
|   |   ├── /migrations
|   |   ├── /seeders
|   ├── /helpers
|   ├── /middlewares
|   ├── /models
|   ├── /routes
|   ├── /Utils
```

## Modelo relacional do banco de dados

![modeloRelacionalMyQuestionAPI](https://user-images.githubusercontent.com/63760217/200000857-9298ec54-9bb1-49ec-bab6-6ae1fd6e9c55.png)