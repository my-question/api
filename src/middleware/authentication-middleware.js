import db from 'models'
import { verifyJWTToken, verifyRefreshToken } from 'helpers'

const Users = db.Users

export const authenticationMiddleware = async (req, res, next) => {
  const { authorization } = req.headers

  if (!authorization) throw new Error('Token não informado')

  const [, token] = authorization.split(' ')
  const { id } = verifyJWTToken(token)

  const { email, ...user } = await Users.findByPk(id)
  if (!user) throw new Error('Não autorizado')

  const loggedUser = { id: id, email: email }
  req.user = loggedUser

  next()
}

export const refreshTokenMiddleware = async (req, res, next) => {
  const { refreshToken } = req.body

  if (!refreshToken) throw new Error('Refresh token não informado')

  const { id } = verifyRefreshToken(refreshToken)

  const { email, ...user } = await Users.findByPk(id)
  if (!user) throw new Error('Não autorizado')

  const loggedUser = { id: id, email: email }
  req.user = loggedUser

  next()
}
