import { URL_FRONT } from 'config'

export const templateForgetPassword = token => {
  const resetLink = `${URL_FRONT}/users/reset`

  return {
    subject: 'Recuperação de senha',
    text: `Acesse o link ${resetLink} e fornece o código: ${token} para redefinir sua senha.`,
    html: `
      <p>Prezado(a), você solicitou a redefinição de senha.</p> <br />
      <p>Acesse o <a href="${resetLink}">sistema</a> e fornece o código: ${token} para redefinir sua senha.</p>
    `
  }
}
