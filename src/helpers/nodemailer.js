import nodemailer from 'nodemailer'

import { SENDER_EMAIL } from 'config'

export const sendEmail = (sendTo, emailBody) => {
  const transporter = nodemailer.createTransport({
    host: 'smtp.mailtrap.io',
    port: 2525,
    auth: {
      user: '7736d73e6389e2',
      pass: '8449a7179e4070'
    }
  })

  const mailOptions = {
    from: SENDER_EMAIL,
    to: sendTo,
    subject: emailBody.subject,
    text: emailBody.text,
    html: emailBody.html
  }

  return transporter.sendMail(mailOptions)
}
