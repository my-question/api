import { sign, verify } from 'jsonwebtoken'
import dayjs from 'dayjs'

import { ACCESS_SECRET, ACCESS_EXPIRES, REFRESH_SECRET } from 'config'

export const generateJWTToken = id => 
  sign({ id: id }, ACCESS_SECRET, { expiresIn: ACCESS_EXPIRES })

export const verifyJWTToken = token => verify(token, ACCESS_SECRET)

export const generateRefreshToken = id =>
  sign({ id: id }, REFRESH_SECRET, { expiresIn: dayjs().add(6, 'hour').unix() })

export const verifyRefreshToken = token => verify(token, REFRESH_SECRET)
