'use strict'
export default {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        allowNull: false
      },
      firstName: { type: Sequelize.STRING(20) },
      lastName: { type: Sequelize.STRING(45) },
      city: { type: Sequelize.STRING(45) },
      state: { type: Sequelize.STRING(20) },
      birthDate: { type: Sequelize.DATEONLY },
      email: {
        type: Sequelize.STRING(45),
        unique: true
      },
      password: { type: Sequelize.STRING(255) },
      passwordResetToken: {
        type: Sequelize.STRING(8),
        unique: true
      },
      createdAt: {
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: { type: Sequelize.DATE }
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users')
  }
}
