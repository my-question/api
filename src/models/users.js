'use strict'
import { Model } from 'sequelize'

export default (sequelize, DataTypes) => {
  class Users extends Model {
    static associate(models) {
      Users.hasOne(models.RefreshToken, { foreignKey: 'userId' })
    }
  }
  Users.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      firstName: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira o nome' },
          len: { args: [3, 20], msg: 'O nome deve conter no mínimo 3 caracters' }
        }
      },
      lastName: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira o sobrenome' },
          len: { args: [3, 45], msg: 'O sobrenome deve conter no mínimo 3 caracters' }
        }
      },
      city: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira a cidade' },
          len: { args: [3, 45], msg: 'A cidade deve conter no mínimo 3 caracters' }
        }
      },
      state: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira o estado' },
          len: { args: [3, 45], msg: 'O estado deve conter no mínimo 3 caracters' }
        }
      },
      birthDate: {
        type: DataTypes.DATEONLY,
        validate: {
          notEmpty: { msg: 'Insira a data de nascimento' }
        }
      },
      email: {
        type: DataTypes.STRING,
        unique: { msg: 'O email deve ser único' },
        validate: {
          isEmail: { msg: 'Email inválido' },
          notEmpty: { msg: 'Insira o email' }
        }
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: { msg: 'Insira a senha' },
          len: {
            args: [8, 256],
            msg: 'A senha deve conter no mínimo 8 caracters'
          }
        }
      },
      passwordResetToken: {
        type: DataTypes.STRING,
        unique: true
      }
    },
    {
      sequelize,
      modelName: 'Users',
      paranoid: true
    }
  )
  return Users
}
