import bcrypt from 'bcrypt'
import crypto from 'crypto'

import db from 'models'
import { 
  generateJWTToken, 
  generateRefreshToken, 
  encryptPassword, 
  sendEmail 
} from 'helpers'
import { templateForgetPassword } from 'utils'

const Users = db.Users
const RefreshToken = db.RefreshToken

export const create = async (req, res) => {
  const { email, password } = req.body

  const [user, created] = await Users.findOrCreate({
    where: { email },
    paranoid: false,
    defaults: {
      email,
      password: await encryptPassword(password)
    }
  })

  if (!created) await user.restore()

  const data = {
    id: user.id,
    email: user.email
  }

  return res.status(201).json(data)
}

export const login = async (req, res) => {
  const { email, password } = req.body

  const user = await Users.findOne({ where: { email } })

  if (!user || !(await bcrypt.compare(password, user.password)))
    throw new Error('Email e/ou senha inválido(s)')

  const [refreshToken, created] = await RefreshToken.findOrCreate({
    where: { userId: user.id },
    defaults: {
      token: generateRefreshToken(user.id),
      userId: user.id
    }
  })

  if (!created) await refreshToken.update({ token: generateRefreshToken(user.id) })

  const data = {
    id: user.id,
    email: user.email,
    accessToken: generateJWTToken(user.id),
    refreshToken: refreshToken.token
  }

  return res.status(200).json(data)
}

export const forget = async (req, res) => {
  const { email } = req.body

  const user = await Users.findOne({ where: { email } })
  if (!user) throw new Error('Email inválido(s)')

  await user.update({ passwordResetToken: crypto.randomBytes(4).toString('HEX') })

  const template = templateForgetPassword(user.passwordResetToken)
  await sendEmail(email, template)

  return res.status(200).end()
}

export const reset = async (req, res) => {
  const { token, password } = req.body

  const user = await Users.findOne({ where: { passwordResetToken: token } })
  if (!user) throw new Error('Código de verificação inválido')

  await user.update({
    password: await encryptPassword(password),
    passwordResetToken: null
  })

  return res.status(200).end()
}

export const refreshToken = async (req, res) => {
  const { id } = req.user

  const refreshToken = await RefreshToken.findOne({ where: { userId: id } })
  await refreshToken.update({ token: generateRefreshToken(id) })

  const data = {
    accessToken: generateJWTToken(id),
    refreshToken: refreshToken.token
  }

  return res.status(200).json(data)
}

export const me = (req, res) => res.json(req.user)

export const show = async (req, res) => {
  const { id } = req.params

  const user = await Users.findByPk(id, {
    attributes: ['firstName', 'lastName', 'city', 'state', 'birthDate', 'email']
  })

  if (!user) throw new Error('Usuário não encontrado')

  return res.status(200).json(user)
}

export const update = async (req, res) => {
  const { id } = req.params
  const { firstName, lastName, city, state, birthDate, email } = req.body

  const user = await Users.findByPk(id)
  await user.update({ firstName, lastName, city, state, birthDate, email })

  return res.status(200).end()
}

export const destroy = async (req, res) => {
  const { id } = req.params

  const user = await Users.findByPk(id)
  if (!user) throw new Error('Usuário não encontrado')

  await user.destroy({ where: { id } })

  return res.status(200).end()
}

export default {
  create,
  login,
  forget,
  reset,
  refreshToken,
  me,
  show,
  update,
  destroy
}
