import { Router } from 'express'
import { authenticationMiddleware, refreshTokenMiddleware } from 'middleware'

import usersController from 'controllers/users-controller'

const routes = Router()

routes.post('/users/signup', usersController.create)
routes.post('/users/login', usersController.login)
routes.post('/users/forget', usersController.forget)
routes.post('/users/reset', usersController.reset)
routes.put('/users/refresh-token', refreshTokenMiddleware, usersController.refreshToken)
routes.get('/me', authenticationMiddleware, usersController.me)
routes.get('/users/:id', authenticationMiddleware, usersController.show)
routes.put('/users/:id', authenticationMiddleware, usersController.update)
routes.delete('/users/:id', authenticationMiddleware, usersController.destroy)

export default routes
